+++
title = "Kazuto NARIMURA"
+++
{{< blocks/cover title="Welcome to my site !!" image_anchor="top" height="full" >}}
<p style="font-size: 1.5rem">Hi, I'm Kazuto NARIMURA, a systems & software engineer.<br>I am also studying mathematics, physics, and informatics.</p>
{{< blocks/link-down >}}
{{< /blocks/cover >}}

<!-- 第1ブロック -->
{{% blocks/lead color="primary" %}}
This is my personal website.<br>
I take notes here so that I can remember later what I have studied.<br>
（Sorry, this website is still only available in Japanese.）
{{% /blocks/lead %}}

<!-- 第2ブロック -->
{{% blocks/section color="white" type="row" %}}

{{% blocks/feature icon="fa-solid fa-address-card" title="About" url="" %}}
[Read more](../ja/about/)
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-book" title="Notes" url="" %}}
[Read more](../ja/notes/)
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-paper-plane" title="Contact" url="" %}}
[Read more](../ja/_index.md#td-block-4)
{{% /blocks/feature %}}

{{% /blocks/section %}}

<!-- 第3ブロック -->
{{% blocks/lead color="primary" %}}
Please feel free to contact me.<br>
{{% /blocks/section %}}

<!-- 第4ブロック -->
{{% blocks/section color="white" type="row" %}}

{{% blocks/feature icon="fa-brands fa-x-twitter" title="" url="" %}}
[Read more](../ja/contact/)
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-brands fa-instagram" title="" url="" %}}
[Read more](../ja/contact/)
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-envelope" title="" url="" %}}
[Read more](../ja/contact/)
{{% /blocks/feature %}}

{{% /blocks/section %}}