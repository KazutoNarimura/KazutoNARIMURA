+++
title = "Contact Me !!"
linkTitle = "Contact"
type = "docs"

[menu.main]
weight = 40
pre = "<i class='fa-solid fa-paper-plane'></i>"
+++
## <i class='fa-brands fa-square-x-twitter fa-lg'></i> X
- [@NarimuraKazuto](https://twitter.com/NarimuraKazuto)

## <i class='fa-brands fa-instagram fa-lg'></i> Instagram
- [narimurakazuto](https://www.instagram.com/narimurakazuto/)

## <i class='fa-brands fa-youtube fa-lg'></i> YouTube
- [@KazutoNARIMURA](https://www.youtube.com/channel/UCI_CVw3wMMJK4nE0-N329zA)