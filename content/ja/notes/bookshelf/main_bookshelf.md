+++
title = "本棚"
linkTitle = "本棚"
type = "docs"
weight = 100
+++
{{% cardpane %}}

{{% card header="**1冊でマスター 大学の微分積分**" title="" subtitle="技術評論社" footer="[https://gihyo.jp/book/2014/978-4-7741-6545-5](https://gihyo.jp/book/2014/978-4-7741-6545-5)" %}}
<div style="text-align: center;">
<img src="mathematics_issatsude_master_calculus.jpg">
</div>
<br>
石井俊全 著
{{% /card %}}

{{% card %}}
{{% /card %}}

{{% card %}}
{{% /card %}}

{{% card %}}
{{% /card %}}

{{% /cardpane %}}

{{% cardpane %}}

{{% card header="**オラクルマスター教科書 Bronze DBA**" title="" subtitle="翔泳社" footer="[https://www.shoeisha.co.jp/book/detail/9784798166360](https://www.shoeisha.co.jp/book/detail/9784798166360)" %}}
<div style="text-align: center;">
<img src="informatics_oracle_master_bronze.jpg">
</div>
<br>
渡辺亮太/岡野平八郎/鈴木俊也 著
{{% /card %}}

{{% card header="**オラクルマスター教科書 Silver DBA**" title="" subtitle="翔泳社" footer="[https://www.shoeisha.co.jp/book/detail/9784798166353](https://www.shoeisha.co.jp/book/detail/9784798166353)" %}}
<div style="text-align: center;">
<img src="informatics_oracle_master_silver.jpg">
</div>
<br>
渡辺亮太/桝井智行/杉本篤信/西田幸平 著
{{% /card %}}

{{% card header="**オラクルマスター教科書 Gold DBA**" title="" subtitle="翔泳社" footer="[https://www.shoeisha.co.jp/book/detail/9784798174365](https://www.shoeisha.co.jp/book/detail/9784798174365)" %}}
<div style="text-align: center;">
<img src="informatics_oracle_master_gold.jpg">
</div>
<br>
渡辺亮太/桝井智行/峯岸隆一 著
{{% /card %}}

{{% card %}}
{{% /card %}}

{{% /cardpane %}}