+++
title = "tree"
linkTitle = "tree"
type = "docs"
weight = 100
+++
## インストール
Ubuntu 22.04.2 LTSへのインストール方法。
```bash
# 確認
snap info tree

# インストール
sudo snap install tree

# 確認
which tree
tree --version
```

## 基本的な使い方
```bash
tree -a -L 2 <Directory-Name>
```

## 主なオプション
|オプション|意味|
|---|---|
|-a|"."から始まる隠しディレクトリや隠しファイルも含めてすべて表示|
|-L <自然数>|表示する深さを指定する|
|-f|パスを付けて表示する|
|-F|ディレクトリの末尾に"/"を付けて表示する|
|-d|ディレクトリのみ表示する|
|-p|パーミッション情報も併せて表示する|

## 参考文献
- 侍エンジニア編集部. "treeでディレクトリの構造を表示する". SAMURAI ENGINEER BLOG. 2024-03-01.
    - https://www.sejuku.net/blog/54248, (参照 2024-04-14).