+++
title = "Windows Subsystem for Linux"
linkTitle = "WSL"
type = "docs"
weight = 5
+++
## Windows Subsystem for Linux とは
- Windows上でLinuxを仮想的に動作させるアプリケーション。
- WSLと略される。
- WSL1からWSL2へアップデートされている。

## Windows Subsystem for Linux のサイト
- 公式サイト（日本語版）
    - [https://learn.microsoft.com/ja-jp/windows/wsl/](https://learn.microsoft.com/ja-jp/windows/wsl/)

## Windows Subsystem for Linux のインストール
- [公式サイトのインストールに方法についての説明](https://learn.microsoft.com/ja-jp/windows/wsl/install)に沿ってインストールする。
- Windows PowerShellを起動する。（管理者権限が必要。）
- オンラインストアからダウンロードできるLinuxディストリビューションの一覧を確認する。
    ```bash
    wsl --list --online
    ```
- Linuxディストリビューションの名前を指定してインストールする。
    ```bash
    wsl --install -d <Linuxディストリビューション名>
    ```
- このあと自動で起動してデフォルトユーザの作成を求められるが、Ctrl+Cなどで停止すれば再び起動した際にrootユーザでログインできる。

## Ubuntuのユーザ作成方法
- ログインシェルの種類・ホームディレクトリ作成・パスワード設定の3点に気を配りながらユーザ作成を行う。
    - adduserコマンドも利用できるが、ユーザ名に大文字が使えないなどの制約がある。
    - 最初に作成したユーザのみがWSLを立ち上げているWindow側のユーザと紐づく。
    ```bash
    useradd -s /bin/bash -m ユーザ名
    passwd ユーザ名 
    ```