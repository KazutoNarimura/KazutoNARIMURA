+++
title = "GitLab"
linkTitle = "GitLab"
type = "docs"
weight = 30
+++
## GitLabとは
- Gitリポジトリを管理するソフトウェアの1つである。
- GitLabの公式サイトによればGitLabはDevSecOpsプラットフォームである。
- サーバにインストールして利用する方法とインターネット上のSaaSとして利用する方法がある。
- 類似のソフトウェアとしてはGitHubがあるが、GitHubはSaaSとしての利用しかできない。

## GitLabのサイト
- 公式サイト
    - [https://about.gitlab.com/](https://about.gitlab.com/)
- 公式ドキュメントサイト
    - [https://docs.gitlab.com/](https://docs.gitlab.com/)

## GitLabの関連サイト
- GitLab認定パートナー「クリエーションライン株式会社」が提供しているGitLab日本語マニュアル
    - [https://gitlab-docs.creationline.com/](https://gitlab-docs.creationline.com/)

## GitLab Pages
### GitLab Pagesとは
- GitLab Pagesを利用すると、GitLabのリポジトリから静的ウェブサイトを直接公開することができる。
- 無料で利用できる。
- 独自に作成したHTML、CSS、JavaScriptをホスティングすることができる。
- 静的サイトジェネレータ（[SSG](../static_site_generator/_index.md)）を使って作成したWebサイトをホスティングすることもできる。

### GitLab Pagesの利用方法
GitLab Pagesの基本的な利用方法を以下に記載する。[Hugo](../static_site_generator/hugo.md)などの静的サイトジェネレータを使って作成したWebサイトをホスティングする場合は、利用した静的サイトジェネレータのドキュメントを参照するほうがよい。
1. 新規プロジェクトを作成
    - 「新規プロジェクト」→「Create blank project」の順に進み、空のプロジェクトを作成する。
    - （テンプレートから作成することはしなくてよい。）
    - （このときのプロジェクト名がWebページのURLに入るので注意。）
2. `.gitlab-ci.yml`ファイルを作成
    - 新たに`.gitlab-ci.yml`という名前のファイルをプロジェクト内に作成する。
    - 中身は次の通りにする。
        ```yaml
        image: alpine:latest

        pages:
            stage: deploy
            script:
            - echo 'Nothing to do...'
            artifacts:
                paths:
                - public
            only:
            - main
        ```
3. publicディレクトリを作成
    - 新たに「public」という名前のディレクトリをプロジェクト内に作成する。
    - この「public」配下のファイル群がWebページとして公開される。
4. publicディレクトリ内にHTMLファイルなどを配置
    - index.htmlを筆頭に、.htmlファイル、.cssファイル、.jsファイルをpublicディレクトリ内に配置する。
5. 公開権限を設定
    - GitLabのプロジェクトの設定ページで「プロジェクトの可視性」を「公開」に設定する。
## 参考文献
- 小泉 健太郎. "GitHubとGitLabの違いをわかりやすく解説。結局、どちらを使えばよい？". カゴヤのサーバー研究室. 2023-10-16.
    - [https://www.kagoya.jp/howto/rentalserver/webtrend/github_gitlab/](https://www.kagoya.jp/howto/rentalserver/webtrend/github_gitlab/), (参照 2024-04-21).