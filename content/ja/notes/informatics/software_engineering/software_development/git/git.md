+++
title = "Git"
linkTitle = "Git"
type = "docs"
weight = 20
+++
## Gitとは
- Gitは分散型バージョン管理システムである。
- プログラムのソースコードの変更履歴を記録・追跡するためのソフトウェアである。
- オープンソースソフトウェアとして無料で利用できる。
- Linuxの開発者であるLinus Torvalds氏が、Linuxのソースコードを管理するためにGitも開発した。

{{% alert title="<i class='fa-solid fa-file-pen fa-sm'></i> Tip" %}}
英語の「git」は「ばか、まぬけ、能なし」といった意味がある。Linus Torvalds氏がGitを開発した際に、自身にちなんだ名前にすることを要請されたことから、半ば皮肉を込めて命名したようである。
{{% /alert %}}

## Gitのサイト
- 公式サイト
    - [https://git-scm.com/](https://git-scm.com/)

## インストール
Ubuntu 22.04.2 LTSへのインストール方法。
```bash
apt-get install git
```

## 初期設定
### デフォルトブランチ名の変更
Gitのメインブランチ名はデフォルトでは`master`になっている。以下のコマンドでこれを`main`に変更する。
```bash
git config --global init.defaultBranch main

# 確認
git config --list
```
既に`master`ブランチが作成されている場合は、そのリポジトリ内で以下のように名前を変更する。
```bash
git branch -m master main

# 確認
git branch
```

### Gitによる管理の開始
ローカルのディレクトリでGitによる管理を開始する方法と、既にGitで管理されているリモートリポジトリをローカルにコピーする方法がある。いずれにせよディレクトリ内に`.git`という隠しディレクトリが作成されており、この中のファイル群によってバージョン管理が行われる。
- ローカルのディレクトリをGit管理にする
    ```bash
    cd <Directory>
    git init
    ```
- リモートリポジトリをローカルにコピーする
    ```bash
    git clone <Remote-Repository's-URL>
    cd <Remote-Repository-Name>
    git remote -v
    ```

### ユーザの登録
最初にユーザの登録を行う。リポジトリごとに別のユーザを登録する方法と、どのリポジトリに対しても同じユーザを登録する方法がある。使い分けは自由であるが、GitLabなどを利用して外部に公開するリポジトリの場合はリポジトリごとに別のユーザを登録する方が安全である。
- リポジトリごとのユーザの登録
    - 以下のコマンドで`<Repository>/.git/config/`というファイルに設定が保存される。
    ```bash
    cd <Repository>
    git config user.name "<Username>"
    git config user.email "<E-Mail-Address>"

    # 確認
    git config --list
    ```
- どのリポジトリにも同じユーザを登録
    - 以下のコマンドで`~/.gitconfig`というファイルに設定が保存される。
    ```bash
    git config --global user.name "<Username>"
    git config --global user.email "<E-Mail-Address>"

    # 確認
    git config --list
    ```

### エディタの登録
コミットメッセージを入力するときにエディタが開く。例えばUbuntuの場合はデフォルトでnanoが開くようになっている。これをvimに変更したい場合は以下のように設定する。
```bash
git config --global core.editor "vim"

# 確認
git config --global --list
```

### 出力の色付け
gitコマンドの出力を自動で色分けするように設定できる。以下のコマンドを実行する。
```bash
git config --global color.ui true

# 確認
git config --global --list
```

## 基本的な使い方
（編集中）

## .gitディレクトリの構成
git initを行うと以下のような構成の`.git`ディレクトリが作成される。
```bash
.git
├── HEAD
├── branches
├── config
├── description
├── hooks
│   ├── applypatch-msg.sample
│   ├── commit-msg.sample
│   ├── fsmonitor-watchman.sample
│   ├── post-update.sample
│   ├── pre-applypatch.sample
│   ├── pre-commit.sample
│   ├── pre-merge-commit.sample
│   ├── pre-push.sample
│   ├── pre-rebase.sample
│   ├── pre-receive.sample
│   ├── prepare-commit-msg.sample
│   ├── push-to-checkout.sample
│   └── update.sample
├── info
│   └── exclude
├── objects
│   ├── info
│   └── pack
└── refs
    ├── heads
    └── tags
```

## 参考文献
- "Git". Wikipedia. 2024-03-08
    - [https://ja.wikipedia.org/wiki/Git](https://ja.wikipedia.org/wiki/Git), (参照 2024-04-21).