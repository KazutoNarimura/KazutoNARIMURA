+++
title = "Linux Distribution"
linkTitle = "Linux"
type = "docs"
weight = 7
+++
## Linux Distribution とは
- LinuxカーネルをOSとして利用できるように様々な他のソフトウェアとともにまとめてパッケージ化したもの。
- Linux Distributionを広義のLinuxとして捉えて単にLinuxと言う場合も多い。
- 厳密にはLinuxはLinuxカーネルのことである。

## Linux Distribution の系統
- 2024年現在の主要なLinuxディストリビューションは大きく2つの系統に分類できる。
    - RedHat系
    - Debian系
- 他にもSlackware系などがあるが、2024年現在はあまり見かけない。

## Linux Distribution の種類
### RedHat系
|Linux Distribution|開発団体|備考|
|---|---|---|
|Fedora|Red Hat, Inc.が支援するコミュニティ「Fedora Project」|先行的検証用の位置づけ|
|CentOS Stream|Red Hat, Inc.が支援するコミュニティ「The CentOS Project」|検証用の位置づけ|
|Red Hat Enterprise Linux (RHEL)|Red Hat, Inc.|企業向けに販売|
|CentOS|Red Hat, Inc.が支援するコミュニティ「The CentOS Project」|2020年代に開発終了|
|Alma Linux|The AlmaLinux OS Foundation|CentOS終了を受けて開始|
|Rocky Linux|Rocky Enterprise Software Foundation|CentOS終了を受けて開始|
|Oracle Linux|Oracle Corporation|RHEL互換|

- RedHat系の各Linux Distributionの関係性
    ```mermaid
    flowchart
        A[Fedora] --> B
        B[CentOS Stream] --> C
        C[Red Hat Enterprise Linux]
        C --> D
        C --> E
        C --> F
        D[Alma Linux]
        E[Rocky Linux]
        F[Oracle Linux]
    ```

### Debian系
|Linux Distribution|開発団体|備考|
|---|---|---|
|Debian GNU/Linux|Debian Project|GNU Projectの精神を尊重|
|Ubuntu|Canonical Ltd.|Debian GNU/Linuxから派生|
|Linux Mint|Clement Lefebvreら||
|elementary OS||macOSに似ている|
- Debian系の各Linux Distributionの関係性
    ```mermaid
    flowchart
        A[Debian GNU/Linux] --> B
        B[Ubuntu]
        B --> C
        B --> D
        C[Linux Mint]
        D[elementary OS]
    ```
## 参考文献
- 末永貴一. "Linux道場入門編：ディストリビューションとは". LPI-JAPAN.
    - [https://lpi.or.jp/lpic_all/linux/intro/intro02.shtml](https://lpi.or.jp/lpic_all/linux/intro/intro02.shtml), (参照 2024-06-16).
- ちびすけ. "Linuxディストリビューションの体系と特徴をまとめてみました". 2023-11-29.
    - [https://note.com/_chibisuke/n/n0ae85ead39cd](https://note.com/_chibisuke/n/n0ae85ead39cd), (参照 2024-06-16).