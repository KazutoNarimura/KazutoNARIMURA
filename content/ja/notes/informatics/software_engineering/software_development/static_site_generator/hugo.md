+++
title = "Hugo"
linkTitle = "Hugo"
type = "docs"
weight = 45
+++
## Hugoとは
- [SSG](_index.md)の1つ。
- Go言語で実装されている。
- レンダリングが高速であることと柔軟性が高いことが強み。

## Hugoのサイト
- 公式サイト
    - [https://gohugo.io/](https://gohugo.io/)

## Hugoの関連サイト
- 個人でHugoのドキュメントを日本語訳して公開してくださっているサイト
    - [https://juggernautjp.info/](https://juggernautjp.info/)
- Hugoのドキュメントを日本語訳するプロジェクト
    - [https://hugojapan.github.io/](https://hugojapan.github.io/)
        - ただし現在（2024年04月）は活動凍結中だと思われる。

## Hugoテーマ
Hugoのコミュニティによって数百種類のHugoテーマが用意されており、これを利用することで洗練されたデザインのサイトを容易に構築できる。用意されているテーマの一覧は[Hugo公式サイト内のHugo Themesのページ](https://themes.gohugo.io/)で見ることができる。なお「Hugo theme ranking」などでGoogle検索すると、どのHugoテーマが人気そうであるかが分かる。

## Hugoのインストール
[Hugo公式サイト内のInstallationのページ](https://gohugo.io/installation/)に記載されている内容に従ってインストールすれば問題ない。公式サイトにも書かれていることだが、Git、Go、DartSassの3つが事前にインストールされている必要がある。

私が2024年4月頃に実際に行った手順を記録しておく。
- インストール環境
    - OSはWindows 10 Pro上のWSL2として動作しているUbuntu 22.04.2 LTS（Jammy Jellyfish）である。
    - Gitは既にインストール済みで、バージョンは2.34.1だった。
        ```bash
        # OSを確認
        cat /etc/os-release

        # Gitを確認
        git version
        ```
- Gitのインストール
    - インストール済みのため不要。
    - 念のためパスを確認。
        ```bash
        # パスを確認
        which git
        ```
- Goのインストール
    - [https://go.dev/dl/](https://go.dev/dl/)へ。
    - 「go1.22.2.linux-amd64.tar.gz」をダウンロード。
    - `/home/<Username>/Hugo/`（`\\wsl.localhost\Ubuntu\home\<Username>\Hugo`）に配置。
    - 解凍してPATHを通す。
        ```bash
        # .tar.gzの配置先へ移動
        cd /home/<Username>/Hugo/

        # 念のため既存のファイルを削除
        sudo rm -rf /usr/local/go

        # 解凍
        sudo tar -C /usr/local -xzf go1.22.2.linux-amd64.tar.gz

        # PATHを通す
        export PATH=$PATH:/usr/local/go/bin

        # 確認
        go version
        ```
- DartSassのインストール
    - Snapを用いてインストールする。
        ```bash
        # snapコマンドでインストール
        sudo snap install dart-sass

        # 確認
        dart-sass --version
        ```
- Hugoのインストール
    - Snapを用いてインストールする。
        ```bash
        # snapコマンドでインストール
        sudo snap install hugo

        # 確認
        hugo version
        ```
    - snapの自動アップデート機能によって何も変更していなくてもhugoが動作しなくなる場合がある。
    - 上記のようになった場合は、snapコマンドで古いバージョンに戻して動作させる。
        ```bash
        # インストール済みのRevを確認する
        sudo snap list hugo --all

        # 切り替える
        sudo snap revert hugo --revision 18951
        ```

## Docsyの利用
Hugoテーマの1つである「Docsy」を利用する場合については[Hugo+Docsy](hugo_and_docsy.md)へ。

## GitLabへのホスティング
[Hugo公式サイト内のHost on GitLab Pagesのページ](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)を参照しながらGitLabへのホスティングを行う。

- 以下の内容の`.gitlab-ci.yml`を`hugo.toml`と同じ場所に作成する
    ```yaml
    variables:
        THEME_URL: github.com/google/docsy@v0.9.1

    image:
        name: registry.gitlab.com/pages/hugo/hugo_extended:0.124.0

    stages:
        - deploy

    default:
        before_script:
            - apk add --no-cache go curl bash npm
            - hugo mod get $THEME_URL
            # To use PostCSS, we must install the following modules. See:
            # - https://gohugo.io/hugo-pipes/postcss/
            # - https://github.com/google/docsy/tree/main#prerequisites
            - npm install postcss postcss-cli autoprefixer
            - npm list

    pages:
        stage: deploy
        script:
            # Build
            - hugo --minify
        artifacts:
            paths:
            - public
        rules:
            - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```
- `hugo.toml`の`baseURL`を設定する
    ```toml
    baseURL = 'https://<Username-in-GitLab>.gitlab.io/<Repository-Name-in-GitLab>'
    ```
- Gitの管理を開始する
    ```bash
    git init
    echo "/public" >> .gitignore
    git config user.name "<Username>"
    git config user.email "<E-Mail-Address>"
    git config --list
    git add .
    git commit -m "Initial Commit!!"
    ```
- GitLabにPushする
    ```bash
    git remote add origin https://gitlab.com/<Username-in-GitLab>/<New-Directory-Name>.git
    git push origin main
    ```

## 参考文献
- "snap で古いバージョンをインストールする". hanhan's blog. 2021-05-03.
    - [https://blog.hanhans.net/2021/05/03/snap-old-version/](https://blog.hanhans.net/2021/05/03/snap-old-version/), (参照 2024-04-21).
- Molina. "Hugo+Docsyで個人Wikiを作る". Molina's Web Site. 2020-02-03.
    - [https://molina.jp/blog/hugo+docsy%E3%81%A6%E5%80%8B%E4%BA%BAwiki%E3%82%92%E4%BD%9C%E3%82%8B/](https://molina.jp/blog/hugo+docsy%E3%81%A6%E5%80%8B%E4%BA%BAwiki%E3%82%92%E4%BD%9C%E3%82%8B/)