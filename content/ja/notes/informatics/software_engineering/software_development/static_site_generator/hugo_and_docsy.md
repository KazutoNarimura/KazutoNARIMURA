+++
title = "Hugo+Docsy"
linkTitle = "Hugo+Docsy"
type = "docs"
weight = 50
+++
## Hugoとは
Hugo自体についての説明は[Hugo](hugo.md)へ。

## Docsyとは
- [Hugoテーマ](hugo.md/#hugoテーマ)の1つ。
- GoogleがGitHub上で公開している。
- 技術文書サイトの作成に必要な機能が十二分に備わっている。

## Docsyのサイト
- Docsyの公式サイト
    - [https://www.docsy.dev/](https://www.docsy.dev/)
- DocsyのGitHubリポジトリ
    - [https://github.com/google/docsy?tab=readme-ov-file](https://github.com/google/docsy?tab=readme-ov-file)

## Docsyの関連サイト
- Goldydocs（=Docsyを使って構築したGoogle公式のサンプルサイト）
    - [https://example.docsy.dev/](https://example.docsy.dev/)
- GoldydocsのGithubリポジトリ
    - [https://github.com/google/docsy-example](https://github.com/google/docsy-example)

## Docsyを使ったサイトの作成
[Docsy公式サイト内の『Use Docsy as Hugo Module』のページ](https://www.docsy.dev/docs/get-started/docsy-as-module/)に記載されている内容に沿ってインストールすれば問題ない。Hugoを動作させるためには、Git、Go、DartSass、Hugoをインストールする必要があった。Docsyを使う場合には、追加でNode.jsとPostCSSも必要となる。

私が2024年4月頃に実際に行った手順を記録しておく。雛形としてサンプルサイトを利用するか、自分で新規に構築するかの2つの選択肢があるようだが、私は新規に構築する方を選択した。

- npmとNode.jsのインストール
    - まずNVM（Node Version Manager）をインストール
    - 続いてnpmとnode.jsをインストール
        ```bash
        # シェルスクリプトを取得してファイルの中身を出力し、bashコマンドに渡して実行する
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash

        # ここでターミナルを再起動する
        # 確認
        nvm -v

        # その後、nvmコマンドでnpmとnode.jsをインストールする
        nvm install 20

        # 確認
        npm -v
        node -v
        ```
- 新規サイトを作成
    - 新規のサイトを作成する
    - このサイト用のディレクトリが作成される
        ```bash
        # ディレクトリを移動
        cd /home/<Username>/

        # hugoコマンドで新規サイト用のディレクトリを作成
        hugo new site <New-Directory-Name>
        ```
- PostCSSのインストール
    - サイト用のディレクトリ内でインストールを行う
        ```bash
        # 作成された新規サイト用のディレクトリへ移動
        cd /home/<Username>/<New-Directory-Name>/

        # npmコマンドでPostCSSをインストール
        npm install -D autoprefixer
        npm install -D postcss-cli
        npm install -D postcss
        ```
- Docsyの導入
    - Hugoモジュールと呼ばれる仕組みを使ってDocsyを導入する
        ```bash
        # サイト用ディレクトリへ移動
        cd /home/<Username>/<New-Directory-Name>

        # サイトをHugoモジュールにする
        # go.modというファイルがディレクトリ内に作成される
        hugo mod init <New-Directory-Name>

        # サイトにDocsyテーマモジュールを導入する
        # go.sumというファイルがディレクトリ内に作成される
        hugo mod get github.com/google/docsy@v0.9.1
        ```
    - `hugo.toml`にDocsyの情報を追記する
        ```bash
        # サイト用ディレクトリへ移動
        cd /home/<Username>/<New-Directory-Name>

        # hugo.tomlを編集
        vim hugo.toml
        ```
        ```toml
        # hugo.toml

        # 以下の記載はデフォルトで存在する
        baseURL = 'https://example.org/'
        languageCode = 'en-us'
        title = 'My New Hugo Site'

        # 以下をファイル末尾に追記
        # 今後このファイルに様々な設定を追記していくことになる
        # 以下の記載はファイル末尾のままとなるように注意する必要がある
        [module]
            proxy = "direct"
            # uncomment line below for temporary local development of module
            # replacements = "github.com/google/docsy -> ../../docsy"
            [module.hugoVersion]
                extended = true
                min = "0.73.0"
            [[module.imports]]
                path = "github.com/google/docsy"
                disable = false
        ```
- Google Analyticsに関する記述の修正
    - Hugoのバージョンアップに伴って動かなくなった部分を修正する
    - 具体的には`/home/<Username>/.cache/hugo_cache/modules/filecache/modules/pkg/mod/github.com/google/docsy@v0.9.1/layouts/partials/head.html`を修正する
    - 以下の部分を削除する
        ```html
        {{/* To comply with GDPR, cookie consent scripts places in head-end must execute before Google Analytics is enabled */ -}}
        {{ if hugo.IsProduction -}}
          {{ $enableGtagForUniversalAnalytics := not .Site.Params.disableGtagForUniversalAnalytics -}}
          {{ if (or $enableGtagForUniversalAnalytics (hasPrefix .Site.GoogleAnalytics "G-")) -}}
            {{ template "_internal/google_analytics_gtag.html" . -}}
          {{ else -}}
            {{ template "_internal/google_analytics_async.html" . -}}
          {{ end -}}
        {{ end -}}
        ```
- サイトのプレビュー
    - 実際にサイトを生成してブラウザで確認する
        ```bash
        # サイト用ディレクトリへ移動
        cd /home/<Username>/<New-Directory-Name>

        # サイトを作成し、ローカルでwebサーバを起動
        hugo server -D
        # ターミナル上で示されるURLにブラウザでアクセスする
        # URLは"http://localhost:1313/<Repository-Name-in-GitLab>"などになる
        # サイトをローカルで確認できる
        ```

## Docsyを使ったサイトの基本的な設定
### HTMLタグを解釈するための設定
- `hugo.toml`に以下を追記する
    ```toml
    # Markdownファイル内のHTMLタグを解釈するための設定
    [markup]
        [markup.goldmark]
            [markup.goldmark.renderer]
                unsafe = true
    ```
### 多言語対応
- `hugo.toml`に以下を追記する
    ```toml
    # 多言語対応
    # Content without language indicator will default to this language. Default is en.
    defaultContentLanguage = "ja"
    # If true, Hugo renders the default language site in a subdirectory matching the defaultContentLanguage. Default is false.
    defaultContentLanguageInSubdir = true
    # If true, auto-detect Chinese/Japanese/Korean Languages in the content. This will make .Summary and .WordCount behave correctly for CJK languages. Default is false.
    hasCJKLanguage = true
    [languages]
        [languages.ja]
            languageName ="日本語"
            # Weight used for sorting.
            weight = 1
            contentDir = "content/ja"
        [languages.ja.params]
            title = "<Site Title in Japanese>"
            description = "<Site Description in Japanese>"
        [languages.en]
            languageName ="English"
            # Weight used for sorting.
            weight = 2
            contentDir = "content/en"
        [languages.en.params]
            title = "<Site Title in English"
            description = "<Site Description in English>"
    ```
### サイト内検索を設置
- `/home/<Username>/<New-Directory-Name>/content/ja/`に以下の内容のファイル`search.md`を追加する
    ```md
    +++
    title = "検索結果"
    layout = "search"
    +++
    ```
- `/home/<Username>/<New-Directory-Name>/content/en/`にも以下の内容のファイル`search.md`を追加する
    ```md
    +++
    title = "Search Results"
    layout = "search"
    +++
    ```
- `hugo.toml`に以下を追記する
    ```toml
    [params]
        # サイト内検索の設定（日本語の検索には未対応！）
        # Enable offline search with Lunr.js
        offlineSearch = true
        offlineSearchSummaryLength = 200
        offlineSearchMaxResults = 25
    ```
- `/home/<Username>/<New-Directory-Name>/assets/_variables_project.scss`を作成
- `/home/<Username>/<New-Directory-Name>/assets/_variables_project.scss`内に以下を追記
    ```scss
    // サイト内検索結果の表示の幅
    .td-offline-search-results {
        min-width: 30%;
    }
    ```
### テーマの色を変更
- `/home/<Username>/<New-Directory-Name>/assets/_variables_project.scss`内に以下を追記
    ```scss
    $primary: <Color-Code>;
    $secondary: <Color-Code>;
    ```
### ヘッダーロゴの表示有無を変更
- `hugo.toml`に以下を追記するとヘッダーのロゴは非表示となる
    ```toml
    [params]
        [params.ui]
            # ロゴの表示有無を設定
            navbar_logo = false
    ```
### コピーライトを設定
- `hugo.toml`に以下を追記する
    ```toml
    [params]
        [params.copyright]
            authors = "<Author's Name>"
            from_year = 2023
    ```
### シンタックスハイライトを設定
- [Prism.js](https://prismjs.com/)を利用する
- [Prism.js](https://prismjs.com/)の「DOWNLOAD」へ
- Themes、Languages、Pluginsから使うものを選択して`prism.js`と`prism.css`をダウンロードする
    - このときPluginsの「Remove initial line feed」を必ず選択すること。
- `/home/<Username>/<New-Directory-Name>/static/js/`に`prism.js`を配置
- `/home/<Username>/<New-Directory-Name>/static/css/`に`prism.css`を配置
- `hugo.toml`に以下を追記する
    ```toml
    [params]
        # Enable syntax highlighting and copy buttons on code blocks with Prism
        prism_syntax_highlighting = true
    ```
### サイドバーの表示設定
- `hugo.toml`に以下を追記する
    ```toml
    [params]
        [params.ui]
            # 左のサイドバーの表示設定
            sidebar_menu_compact = true
            ul_show = 1
            sidebar_menu_foldable = false
            sidebar_cache_limit = 1000
    ```
### 警告表示ブロックに背景色をつける
- `/home/<Username>/<New-Directory-Name>/assets/`に`_alerts.scss`を作成
- `/home/<Username>/<New-Directory-Name>/assets/_alerts.scss`に以下を追記
    ```scss
    .alert {
        font-weight: $font-weight-medium;
        // 以下のコメントアウトが重要
        // background: $white;
        color: inherit;
        border-radius: 0;

        @each $color, $value in $theme-colors {
            &-#{$color} {
            & .alert-heading {
                color: $value;
            }

            border-style: solid;
            border-color: $value;
            border-width: 0 0 0 4px;
            }
        }
    }
    ```
### リンクテキストを適切に折り返す
- `/home/<Username>/<New-Directory-Name>/assets/_variables_project.scss`に以下を追記
    ```scss
    // URLやリンクテキストを改行させる
    a {
        word-break: break-all;
    }
    ```

## Docsyにおけるコンテンツ作成方法
`/home/<Username>/<New-Directory-Name>/content/ja/`や`/home/<Username>/<New-Directory-Name>/content/en/`にディレクトリやファイルを配置していけばよい。例えば以下のようなディレクトリ構造にするとよい。
```bash
content
├── en/
│   ├── _index.md
│   └── search.md
└── ja/
    ├── _index.md
    ├── about/
    ├── blog/
    ├── contact/
    ├── docs/
    ├── featured-background.jpg
    └── search.md
```
さらに`/home/<Username>/<New-Directory-Name>/content/ja/about/`に`_index.md`および`icon_for_profile.png`を配置するなどを行う。
```bash
content/ja/about/
├── _index.md
└── icon_for_profile.png
```
また`/home/<Username>/<New-Directory-Name>/content/ja/docs/`に`_index.md`および従属するコンテンツを配置するなどを行う。画像ファイルを扱いたいときは配置する場所に注意すること。
```bash
content/ja/docs/
├── _index.md
├── AAA/
│   ├── _index.md
│   └── AAA_1.md
├── BBB/
│   ├── _index.md
│   ├── BBB_1.md
│   ├── BBB_2.md
│   ├── BBB_3.md
│   ├── BBB_3/image_for_BBB_3.png
│   └── BBB_4.md
└── CCC/
    ├── _index.md
    ├── CCC_XXX/
    │   └── _index.md
    ├── CCC_YYY/
    │   └── _index.md
    └── CCC_ZZZ/
        ├── ZZZ_1.md
        └── ZZZ_2.md
```
コンテンツとして含めるMarkdownファイル（.mdファイル）には「Front matter（前付け）」と呼ばれるものを冒頭に記載する。これは文書のメタデータを記載するものであり、中身としては文書のタイトルや文書の種類などがある。TOML形式、YAML形式、JSON形式のいずれかで記載する。TOML形式の場合は`+++`の行で挟まれた間の行がFront matterとなる。`/home/<Username>/<New-Directory-Name>/content/ja/about/_index.md`のFront matterの書き方の例は以下の通りである。なお、この例のように`[menu.main]`が記載されているとヘッダーにリンクが表示される。
```md
+++
title = "About Me"
linkTitle = "About"
type = "docs"

[menu.main]
weight = 10
pre = "<i class='fa-solid fa-address-card'></i>"
+++
```
ヘッダーにリンクを表示しない場合のFront matterの例は以下の通りである。
```md
+++
title = "Linux Commands"
linkTitle = "Linux Commands"
type = "docs"
weight = 10
+++
```
Front matterに関する詳細は[Docsy公式サイト内の『Adding Content』のページ](https://www.docsy.dev/docs/adding-content/content/)を参照すること。

## Docsyにおけるコンテンツ記述方法
### Markdown記法とShortcode記法
基本的には一般的なMarkdownの記法に則って記述すれば問題ない。さらにDocsyではMarkdownの記法による記述の他に「Docsy Shortcodes」という機能を使った記述が可能である。[Docsy公式サイト内の『Docsy Shortcodes』のページ](https://www.docsy.dev/docs/adding-content/shortcodes/)
を参照すること。Shortcodeは`{{%/* .... */%}}`と`{{</* .... */>}}`の2通りの記述方法があり原則としてどちらを用いてもよいが、コードを記載するときは`{{</* .... */>}}`を使わなければならない。

### 警告
通知や警告を表示するための機能としてalert shortcodeがある。以下に3つの記述例を載せておく。また[Bootstrap公式サイト内の『Theme Colors』の項目](https://getbootstrap.com/docs/5.3/customize/color/#theme-colors)も参考になる。
- Warning
    ```md
    {{%/* alert title="<i class='fa-solid fa-circle-exclamation fa-sm'></i> Warning" color="warning" */%}}
    This is a warning.
    {{%/* /alert */%}}
    ```
    {{% alert title="<i class='fa-solid fa-circle-exclamation fa-sm'></i> Warning" color="warning" %}}
This is a warning.
    {{% /alert %}}
- Success
    ```md
    {{%/* alert title="<i class='fa-solid fa-circle-check fa-sm'></i> Success" color="success" */%}}
    Success!
    {{%/* /alert */%}}
    ```
    {{% alert title="<i class='fa-solid fa-circle-check fa-sm'></i> Success" color="success" %}}
Success!
    {{% /alert %}}
- Tip
    ```md
    {{%/* alert title="<i class='fa-solid fa-file-pen fa-sm'></i> Tip" */%}}
    Tipは店員に支払うチップの意味もあるが、有益な情報という意味もある。
    {{%/* /alert */%}}
    ```
    {{% alert title="<i class='fa-solid fa-file-pen fa-sm'></i> Tip" %}}
Tipは店員に支払うチップの意味もあるが、有益な情報という意味もある。
    {{% /alert %}}

### 画像
画像を貼り込むときはimgproc shortcodeが便利である。画像処理の方法やオプションはHugo公式ドキュメント内の[『Image processing methods』](https://gohugo.io/content-management/image-processing/#image-processing-methods)と[『Image processing options』](https://gohugo.io/content-management/image-processing/#image-processing-options)の項目を参照すること。
```md
{{%/* imgproc Google_from_Unsplash.jpg Fill "640x427" */%}}
Google<br>
出典：[Unsplash](https://unsplash.com/ja/%E5%86%99%E7%9C%9F/%E3%82%B0%E3%83%BC%E3%82%B0%E3%83%AB%E3%82%B5%E3%82%A4%E3%83%B3-fpZZEV0uQwA)
{{%/* /imgproc */%}}
```
{{% imgproc Google_from_Unsplash.jpg Fill "640x427" %}}
Google<br>
出典：[Unsplash](https://unsplash.com/ja/%E5%86%99%E7%9C%9F/%E3%82%B0%E3%83%BC%E3%82%B0%E3%83%AB%E3%82%B5%E3%82%A4%E3%83%B3-fpZZEV0uQwA)
{{% /imgproc %}}

### タブ区画
tabpane shortcodeによってタブ区画を作成することができる。タブ区画ではタブごとに表示を切り替えることができる。
```md
{{</* tabpane text=false right=false */>}}
    {{</* tab header="**C++**" lang="cpp" */>}}
#include <iostream>

int main()
{
std::cout << "Hello, World!" << std::endl;
}{{</* /tab >}}
    {{</* tab header="**Python**" lang="py" */>}}
print("Hello, World!"){{</* /tab */>}}
{{</* /tabpane */>}}
```
{{< tabpane text=false right=false >}}
    {{< tab header="**C++**" lang="cpp" >}}
#include <iostream>

int main()
{
std::cout << "Hello, World!" << std::endl;
}{{< /tab >}}
    {{< tab header="**Python**" lang="py" >}}
print("Hello, World!"){{< /tab >}}
{{< /tabpane >}}

### カード区画
cardpane shortcodeによってカードのような表示ができる。通常のテキストの場合は以下のようになる。
```md
{{%/* cardpane */%}}
{{%/* card header="**Isaac Newton**" title="A physicist" subtitle="Newtonian mechanics" footer="<img src='Isaac_Newton_signature_ws.png'>" */%}}
<div style="text-align: center;">
<img src="Isaac_Newton_1689_brightened.jpg">
<p style="font-size: 1.2rem">1642/12/25 - 1726/03/20</p>
</div>
{{%/* /card */%}}
{{%/* card header="**James Clerk Maxwell**" title="A physicist" subtitle="The classical theory of electromagnetic radiation" footer="<img src='James_Clerk_Maxwell_sig.png'>" */%}}
<div style="text-align: center;">
<img src="James_Clerk_Maxwell_in_his_40s.jpg">
<p style="font-size: 1.2rem">1831/01/13 - 1879/11/05</p>
</div>
{{%/* /card */%}}
{{%/* card header="**Albert Einstein**" title="A physicist" subtitle="The theory of relativity" footer="<img src='Albert_Einstein_signature_1934.png'>" */%}}
<div style="text-align: center;">
<img src="Einstein_1921.jpg">
<p style="font-size: 1.2rem">1879/03/14 - 1955/04/18</p>
</div>
{{%/* /card */%}}
{{%/* /cardpane */%}}
```
{{% cardpane %}}
{{% card header="**Isaac Newton**" title="A physicist" subtitle="Newtonian mechanics" footer="<img src='Isaac_Newton_signature_ws.png'>" %}}
<div style="text-align: center;">
<img src="Isaac_Newton_1689_brightened.jpg">
<p style="font-size: 1.2rem">1642/12/25 - 1726/03/20</p>
</div>
{{% /card %}}
{{% card header="**James Clerk Maxwell**" title="A physicist" subtitle="The classical theory of electromagnetic radiation" footer="<img src='James_Clerk_Maxwell_sig.png'>" %}}
<div style="text-align: center;">
<img src="James_Clerk_Maxwell_in_his_40s.jpg">
<p style="font-size: 1.2rem">1831/01/13 - 1879/11/05</p>
</div>
{{% /card %}}
{{% card header="**Albert Einstein**" title="A physicist" subtitle="The theory of relativity" footer="<img src='Albert_Einstein_signature_1934.png'>" %}}
<div style="text-align: center;">
<img src="Einstein_1921.jpg">
<p style="font-size: 1.2rem">1879/03/14 - 1955/04/18</p>
</div>
{{% /card %}}
{{% /cardpane %}}

cardpane shortcodeでコードを書く場合は以下のようになる。
```md
{{</* card code=true header="**C**" lang="C" */>}}
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  puts("Hello World!");
  return EXIT_SUCCESS;
}
{{</* /card */>}}
```
{{< card code=true header="**C**" lang="C" >}}
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  puts("Hello World!");
  return EXIT_SUCCESS;
}
{{< /card >}}

### 数式
特に新たに設定を行わずとも[\\(\KaTeX\\)](https://katex.org/)によって数式が記述できる。ただしデフォルトの設定では数式は`1.2em`で表示されるため少し小さく感じる。これを変更するには`/home/<Username>/<New-Directory-Name>/assets/scss/_variables_project.scss`内に以下を追記する方法が考えられる。
```scss
// KaTeX
.katex {
    // 数式や化学式のサイズを強制的に変更
    font: normal 1.4em KaTeX_Main, Times New Roman, serif !important;
```
またスマートフォンで閲覧するときのように画面幅が狭い場合には数式や化学式が不自然に右側にはみ出すような形で表示されてしまう。これを改善するには`/home/<Username>/<New-Directory-Name>/assets/scss/_variables_project.scss`内に以下のように追記する方法が考えられる。
```scss
// KaTeX
.katex {
    // 数式や化学式のサイズを強制的に変更
    font: normal 1.4em KaTeX_Main, Times New Roman, serif !important;
    // 画面幅に収まらない数式や化学式はスクロールバーで表示する
    overflow-x: auto;
    overflow-y: hidden;
}
```
さらに、他のコンテンツが表示可能な幅の80%で表示されるよう制限されているのに対して、数式や化学式にはこの制限が適用されない。そのため中心位置が不揃いとなってしまう。これを防ぐために`/home/<Username>/<New-Directory-Name>/assets/scss/_variables_project.scss`に以下を追記する。
```scss
// 数式や化学式を表示する際の中心位置を他の要素と合わせる
.td-content > .math, .chem {
    max-width: 80%;
}
```
#### テキストスタイルのインライン数式
```md
質量とエネルギーの等価性は \\(E=mc^{2}\\) という有名な公式として広く知られています。
```
質量とエネルギーの等価性は \\(E=mc^{2}\\) という有名な公式として広く知られています。

#### ディスプレイスタイルのディスプレイ数式
````md
```math
e^{i\pi} + 1 = 0
```
```math
\int_{-\infty}^{\infty} e^{-x^{2}} dx = \sqrt{\pi}
```
````
```math
e^{i\pi} + 1 = 0
```
```math
\int_{-\infty}^{\infty} e^{-ax^{2}} dx = \sqrt{\frac{\pi}{a}}
```

### 化学式
`hugo.toml`に以下を追記することにより、数式と同様に[\\(\KaTeX\\)](https://katex.org/)によって化学式を記述できる。
```toml
[params]
    [params.katex]
    enable = true
        [params.katex.mhchem]
        enable = true
```
#### テキストスタイルのインライン化学式
```md
ハーバー・ボッシュ法は \\( \ce{N2 + 3H2 <=> 2NH3 + \pu{92kJ}} \\) という化学式で表せます。
```
ハーバー・ボッシュ法は \\( \ce{N2 + 3H2 <=> 2NH3 + \pu{92kJ}} \\) という化学式で表せます。

#### ディスプレイスタイルのディスプレイ化学式
````md
```chem
\ce{2NH4Cl + Ca(OH)2 -> CaCl2 + 2NH3 ^ + 2H2O}
```
````
```chem
\ce{2NH4Cl + Ca(OH)2 -> CaCl2 + 2NH3 ^ + 2H2O}
```

### ダイアグラム
[Mermaid](https://mermaid.js.org/)によってダイアグラムを記述できる。`hugo.toml`に以下を追記する。
```toml
[params]
    [params.mermaid]
        version = "10.9.0"
```
Mermaidの記述方法の詳細は[Mermaid公式サイト内のDocsのページ](https://mermaid.js.org/intro/)を参照すること。
````md
```mermaid
graph TD;
    A --> B;
    B --> C;
    B --> D;
    C --> E;
    D --> E;
```
````
```mermaid
graph TD;
    A --> B;
    B --> C;
    B --> D;
    C --> E;
    D --> E;
```
````md
```mermaid
%%{init:{'theme':'forest'}}%%
sequenceDiagram
    autonumber
    Client ->> Server: Syn
    Server ->> Client: Syn/Ack
    Client ->> Server: Ack
```
````
```mermaid
%%{init:{'theme':'forest'}}%%
sequenceDiagram
    autonumber
    Client ->> Server: Syn
    Server ->> Client: Syn/Ack
    Client ->> Server: Ack
```

## 参考文献
- shin. "curlコマンドの-o-オプションとは何か？意味を実例で解説". Prograshi. 2024-02-06.
    - [https://prograshi.com/general/command/curl-o-and-wget-qo/](https://prograshi.com/general/command/curl-o-and-wget-qo/), (参照 2024-04-10).
- "URLやリンクのテキストが改行されずレイアウトが崩れた時の対処法". KNOWLEDGE BASE. 2022-02-25.
    - [https://knowledge-base.site/web_creative/web_creative-922/](https://knowledge-base.site/web_creative/web_creative-922/), (参照 2024-05-06).
- くろえい. "Hugo の Blowfish で KaTeX のはみ出した分を表示しスクロールバーを無くす". くろえいさんのサイト. 2024-02-22
    - [https://kuroyei.com/blog/2024/katex_blowfish_overflow_visible/](https://kuroyei.com/blog/2024/katex_blowfish_overflow_visible/), (参照 2024-05-10).
- "Isaac Newton". Wikipedia. 2024-04-09
    - [https://en.wikipedia.org/wiki/Isaac_Newton](https://en.wikipedia.org/wiki/Isaac_Newton), (参照 2024-04-14).
- "James Clerk Maxwell". Wikipedia. 2024-04-13.
    - [https://en.wikipedia.org/wiki/James_Clerk_Maxwell](https://en.wikipedia.org/wiki/James_Clerk_Maxwell), (参照 2024-04-14).
- "Albert Einstein". Wikipedia. 2024-03-29.
    - [https://en.wikipedia.org/wiki/Albert_Einstein](https://en.wikipedia.org/wiki/Albert_Einstein), (参照 2024-04-14).