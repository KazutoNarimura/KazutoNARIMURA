+++
title = "Astro"
linkTitle = "Astro"
type = "docs"
weight = 55
+++
## Astroとは
- [SSG](_index.md)の1つ。
- TypeScript言語で実装されている。
- クライアントサイドのJavaScriptを極力減らして高速化していることが強み。

## Astroのサイト
- 公式サイト
    - [https://astro.build/](https://astro.build/)

## Astroのインストール
- インストール環境
    - OS：Ubuntu 22.04.2 LTS
- nodeの確認
    ```bash
    # nodeがインストールされていることを確認
    which node

    # nodeのバージョンを確認
    node -v
    ```
- npmの確認
    ```bash
    # npmがインストールされていることを確認
    which npm

    # npmのバージョンを確認
    npm -v
    ```
- プロジェクトを作成
    ```bash
    # 移動
    cd /home/<Username>/

    # プロジェクトの作成を開始
    npm create astro@latest

    # Launch sequence initiated.

    # Where should we create your new project?
    /home/<Username>/<New-Directory-Name>
    
    # How would you  like to start your new project?
    # ・Include sample files (recommended)
    # ・Use blog template
    # ・Empty
    Include sample files

    # Do you plan to write TypeScript?
    # ・Yes
    # ・No
    Yes

    # How strict should TypeScript be?
    # ・Strict (recommended)
    # ・Strictest
    # ・Relaxed
    Strict

    # Install dependencies? (recommended)
    # ・Yes
    # ・No
    Yes

    # Initialize a new git repository? (optional)
    # ・Yes
    # ・No
    Yes

    # Liftoff confirmed. Explore your project!

    # Enter your project directory using cd ./AstroTest 
    # Run npm run dev to start the dev server. CTRL+C to stop.
    # Add frameworks like react or tailwind using astro add.

    # ╭─────╮    Houston:
    # │ ◠ ◡ ◠  Good luck out there, astronaut! 🚀
    # ╰─────╯
    ```
    {{% alert title="<i class='fa-solid fa-file-pen fa-sm'></i> Tip" %}}
アメリカのMajor League Baseballに「Houston Astros」というプロ野球球団がある。Texas州のHoustonという都市を本拠地とする球団である。HoustonにはLyndon B. Johnson Space CenterというNASAの宇宙センターの1つがあることから、「astronaut（宇宙飛行士）」を短縮して「Astros」を球団名とした経緯があるようだ。
    {{% /alert %}}
- 必要なパッケージのインストール
    ```bash
    # 移動
    cd /home/<Username>/<Directory-Name>
    
    # パッケージのインストール
    npm install
    ```

## Astroの起動
- Astroをローカル環境で起動する。
    ```bash
    # 移動
    cd /home/<Username>/<Directory-Name>

    # 起動する
    npm run dev
    ```
- ブラウザで以下のURLにアクセスすると表示される。
    - `http://localhost:4321/`

## エディタの設定
- [Astro公式サイトのページ](https://docs.astro.build/en/editor-setup/)を参照してエディタの設定を行う。
- AstroはエディタとしてVS Codeを利用することを推奨している。
- VSCodeの拡張機能「[Astro](https://marketplace.visualstudio.com/items?itemName=astro-build.astro-vscode)」を入れる。
- .astro形式のファイルにシンタックスハイライトがつくという面だけ見ても便利。

## AstroのDev Toolbar
- Astroをローカルで起動すると[Dev Toolbar](https://docs.astro.build/en/guides/dev-toolbar/)というものがデフォルトで有効になっている。
- 開発のためのツールであり、公開するサイトには表示されない。
- 無効化することもできる。
- 基本的なパフォーマンスとアクセシビリティをチェックして、問題を通知してくれる。

## Astro Projectのディレクトリ構造
- Astro Projectのディレクトリには主に以下のディレクトリとファイルがある。
    - `astro.config.mjs`
    - `package.json`
    - `public/*`
    - `src/*`
    - `tsconfig.json`
- 他にもディレクトリやファイルは存在する。
    - 以下のように`tree`コマンドで確認できる。
        ```bash
        cd /home/<Username>
        tree -a -L 1 <Directory-Name>
        ```
    - `tree`コマンドの結果は以下のようになっていた。
        ```bash
        <Directory-Name>
        ├── .astro
        ├── .git
        ├── .gitignore
        ├── .vscode
        ├── README.md
        ├── astro.config.mjs
        ├── node_modules
        ├── package-lock.json
        ├── package.json
        ├── public
        ├── src
        └── tsconfig.json
        ```
- `src/`ディレクトリはAstroがビルド時に処理するファイル群を格納する。`src/`ディレクトリには主に以下のサブディレクトリがある。
    - `src`
        - `components`
            - プロジェクトのすべてのComponentsをこのディレクトリに格納する。ComponentsとはHTMLのページで再利用可能なコードのまとまりを指す。Astro Components、あるいはReactやVueのようなUIフレームワークなどである。
        - `content`
            - Content Collection Directory（=Content Collections）のみを格納するディレクトリ。初期状態では何も入っていない。なお、Content Collection Directoryの中にはCollection Entryと呼ぶファイル（.md形式、.mdx形式、.yaml形式、.json形式のいずれかの形式のファイル）を入れる。
        - `layouts`
            - Layout Componentsを格納するディレクトリ。Layout Componentsとはページテンプレートのような再利用可能なUI構造を提供するために使用されるAstro Components（.astro形式のファイル）である。
        - `pages`
            - Pages Componentsを格納するディレクトリ。Pages ComponentsとはルーティングやデータロードやWebサイト全体のページレイアウトを処理する役割を担う.astro形式または.md形式のファイルである。このディレクトリは必須のディレクトリである。
        - `styles`
            - CSSやSassのファイルを格納するディレクトリ。
- `public/`ディレクトリにはAstroがビルド時に処理する必要のないファイル群を格納する。具体的には画像やフォント、robots.txt、manifest.webmanifestなどのファイルを格納する。CSSやJavaScriptは`public`に置くこともできるが、ビルド時に最適化されない。そのため、自分で書いた.cssファイルや.jsファイルは`src/`ディレクトリに配置することが推奨されている。
