+++
title = "Package Manager"
linkTitle = "Package Manager"
type = "docs"
weight = 9
+++
## Package Manager とは
- パッケージのインストールや依存関係の解決などの管理を行うシステムである。
- パッケージ管理システムとも呼ぶ。

## Package Manager のまとめ
|OS|パッケージ|パッケージ管理システム|パッケージ管理システム（依存関係解決も行う拡張版）|
|---|---|---|---|
|RedHat系|.rpm形式|RPM|YUM, DNF|
|Debian系|.deb形式|dpkg|APT|
|Linux（Distributionに非依存）|||Snap|
|macOS|||homebrew|
|Windows|||Windows Package Manager, Chocolatey|