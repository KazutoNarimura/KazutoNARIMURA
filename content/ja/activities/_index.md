+++
title = "My activities"
linkTitle = "Activities"
type = "docs"

[menu.main]
weight = 30
pre = "<i class='fa-solid fa-person-hiking'></i>"
+++
## [KazutoNARIMURA](https://kazutonarimura.gitlab.io/KazutoNARIMURA)
- 個人サイト。（このサイト。）
- GitLabリポジトリは以下の場所にあります。
    - <i class="fa-brands fa-square-gitlab"></i> [https://gitlab.com/KazutoNarimura/KazutoNARIMURA](https://gitlab.com/KazutoNarimura/KazutoNARIMURA)