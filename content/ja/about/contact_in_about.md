+++
title = "連絡先"
linkTitle = "連絡先"
type = "docs"
hide_summary = true
weight = 60
+++
<div>
    <a class="btn btn-primary me-3 mb-4" href="../../contact">
    Contact <i class="fas fa-arrow-alt-circle-right ms-2"></i>
    </a>
</div>

- 連絡先については[Contact](../contact)へ