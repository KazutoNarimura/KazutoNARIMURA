+++
title = "略歴"
linkTitle = "略歴"
type = "docs"
hide_summary = true
weight = 40
+++
- 関西出身
- 公立高校 卒業
- 京都大学 工学部 卒業
- 京都大学 大学院 情報学研究科 修士課程 修了
- IT系企業 社員（2024年 現在）