+++
title = "資格"
linkTitle = "資格"
type = "docs"
hide_summary = true
weight = 50
+++
取得した資格や合格した試験の一部をここに記載する。
- 準中型自動車免許
- 基本情報技術者試験
- 応用情報技術者試験
- Certified Kubernetes Administrator（CKA）
- JDLA Deep Learning for GENERAL（G検定）