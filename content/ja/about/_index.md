+++
title = "About Me"
linkTitle = "About"
type = "docs"

[menu.main]
weight = 10
pre = "<i class='fa-solid fa-address-card'></i>"
+++
## 成村 一斗（Kazuto NARIMURA）
{{% imgproc icon_for_about_transparent.png Resize "150x" %}}{{% /imgproc %}}
東京都在住の会社員。学生時代に情報学を学んだ後、IT系企業に就職。システム設計やソフトウェア開発などに従事している。仕事の傍ら数学や物理学など様々な分野の勉強を行っており、このサイトにメモを取っている。

- [活動](activities_in_about)
- [略歴](brief_personal_history)
- [資格](licenses_and_certifications)
- [連絡先](contact_in_about)